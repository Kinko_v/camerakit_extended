package com.synergy.camerakit_extended.demo;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.CamcorderProfile;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.synergy.camerakit_extended.CameraKit;
import com.synergy.camerakit_extended.CameraListener;
import com.synergy.camerakit_extended.CameraView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.activity_main)
    ViewGroup parent;

    @BindView(R.id.camera)
    CameraView camera;

    @BindView(R.id.focusMarker)
    FocusMarkerLayout focusMarker;

    // Capture Mode:

    @BindView(R.id.capturePhoto)
    AppCompatImageButton capturePhoto;
    @BindView(R.id.toggleCamera)
    AppCompatImageButton toggleCamera;
    @BindView(R.id.captureVideo)
    AppCompatImageButton captureVideo;

    @BindView(R.id.captureModeRadioGroup)
    RadioGroup captureModeRadioGroup;

    // Zoom Mode:

    @BindView(R.id.zoomModeRadioGroup)
    RadioGroup zoomModeRadioGroup;
    @BindView(R.id.edit_zoom)
    EditText edit_zoom;
    @BindView(R.id.set_zoom_button)
    Button set_zoom;

    //video settings

    @BindView(R.id.videoMode)
    RadioGroup qualityModeRadioGroup;

    // focus Mode:

    @BindView(R.id.focusModeRadioGroup)
    RadioGroup focusModeRadioGroup;

    // Crop Mode:

    @BindView(R.id.cropModeRadioGroup)
    RadioGroup cropModeRadioGroup;

    @BindView(R.id.zoom_seekbar)
    SeekBar zoom_seekBar;

    @BindView(R.id.text_max_time)
    TextView text_max_time;
    @BindView(R.id.time_seekbar)
    SeekBar time_seekbar;

    @BindView(R.id.text_bitrate)
    TextView text_bitrate;
    @BindView(R.id.bitrate_seekbar)
    SeekBar bitrate_seekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        captureModeRadioGroup.setOnCheckedChangeListener(captureModeChangedListener);
        zoomModeRadioGroup.setOnCheckedChangeListener(zoomModeChangedListener);
        focusModeRadioGroup.setOnCheckedChangeListener(focusModeChangedListener);
        cropModeRadioGroup.setOnCheckedChangeListener(cropModeChangedListener);
        qualityModeRadioGroup.setOnCheckedChangeListener(qualityModeChangedListener);

        /**
         * Library allows to set custom frame size, framerate, bitrate and media format,
         * but it will override video profile setting. You have to set OutputFormat
         * to use setVideoFrameSize or setVideoFramerate methods.
         */
//        camera.setVideoOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//        camera.setVideoFrameSize(320, 240);
//        camera.setVideoFramerate(15);

        camera.setVideoProfile(CamcorderProfile.QUALITY_480P);
        camera.setFocus(CameraKit.Constants.FOCUS_OFF);
        camera.setZoom(CameraKit.Constants.ZOOM_OFF);

        time_seekbar.setMax(120);
        time_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress<3) progress = 3;
                camera.setMaximumRecordingTime(progress*1000);
                text_max_time.setText(progress+ " s");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        time_seekbar.setProgress(60);

        bitrate_seekbar.setMax(5800);
        bitrate_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                camera.setVideoBitrate((progress+300)*1000);
                text_bitrate.setText((progress+300)+ " kbps");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        bitrate_seekbar.setProgress(2200);

        int permissionCheck = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        100);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.start();
        capturePhoto.setColorFilter(ActivityCompat.getColor(MainActivity.this, android.R.color.white));
        capturePhoto.setEnabled(true);
        captureVideo.setColorFilter(ActivityCompat.getColor(MainActivity.this, android.R.color.white));
        captureVideo.setEnabled(true);
    }

    @Override
    protected void onPause() {
        camera.stop();
        super.onPause();
    }

    private Bitmap fixFlip(Bitmap img){
        Matrix rotateRight = new Matrix();
        rotateRight.preRotate(90);

        if(android.os.Build.VERSION.SDK_INT>13 && camera.getFacing() == CameraKit.Constants.FACING_FRONT)
        {
            float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1};
            rotateRight = new Matrix();
            Matrix matrixMirrorY = new Matrix();
            matrixMirrorY.setValues(mirrorY);

            rotateRight.postConcat(matrixMirrorY);

            rotateRight.preRotate(270);

        }

        final Bitmap rImg= Bitmap.createBitmap(img, 0, 0,
                img.getWidth(), img.getHeight(), rotateRight, true);
        return rImg;
    }

    @OnClick(R.id.capturePhoto)
    void capturePhoto() {
        final long startTime = System.currentTimeMillis();
        camera.setCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);
                long callbackTime = System.currentTimeMillis();
                Bitmap bitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
                bitmap = fixFlip(bitmap);
                ResultHolder.dispose();
                ResultHolder.setImage(bitmap);
                ResultHolder.setNativeCaptureSize(
                        captureModeRadioGroup.getCheckedRadioButtonId() == R.id.modeCaptureStandard ?
                                camera.getCaptureSize() : camera.getPreviewSize()
                );
                ResultHolder.setTimeToCallback(callbackTime - startTime);
                Intent intent = new Intent(MainActivity.this, PreviewActivity.class);
                startActivity(intent);
            }
        });
        camera.captureImage();
        capturePhoto.setColorFilter(Color.parseColor("#999999"));
        capturePhoto.setEnabled(false);
        captureVideo.setColorFilter(Color.parseColor("#999999"));
        captureVideo.setEnabled(false);
    }

    @OnClick(R.id.captureVideo)
    void captureVideo() {
        if (!camera.isRecording()) {
            camera.setCameraListener(new CameraListener() {
                @Override
                public void onVideoTaken(final File video) {
                    if (video != null) {
                        /**
                         * You can do anything with resulting video. In this demo I'm asking user
                         * to select a folder to save video in. After that I'm saving video in
                         * new file with unique time name.
                         */
                        Toast.makeText(MainActivity.this, "Choose a folder to store video in\n"+video.length() / 1024 + " kb\n" + camera.getCurrentRecordTime()/1000f + " s", Toast.LENGTH_LONG).show();
                        OpenFileDialog dialog = new OpenFileDialog(MainActivity.this);
                        dialog.setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {
                            @Override
                            public void OnSelectedFile(String fileName) {
                                File fine = new File(fileName + "/" + new Date().getTime() + ".mp4");
                                try {
                                    InputStream in = new FileInputStream(video);
                                    OutputStream out = new FileOutputStream(fine);

                                    // Transfer bytes from in to out
                                    byte[] buf = new byte[1024];
                                    int len;
                                    while ((len = in.read(buf)) > 0) {
                                        out.write(buf, 0, len);
                                    }
                                    in.close();
                                    out.close();
                                    video.delete();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(MainActivity.this, "Video saved as "+fine.getAbsolutePath(), Toast.LENGTH_LONG).show();
                            }
                        });
                        try {
                            dialog.show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
            capturePhoto.setColorFilter(Color.parseColor("#999999"));
            capturePhoto.setEnabled(false);
            toggleCamera.setColorFilter(Color.parseColor("#999999"));
            toggleCamera.setEnabled(false);
            camera.startRecordingVideo();
            Toast.makeText(MainActivity.this, "Tap again to stop recording!", Toast.LENGTH_SHORT).show();
        } else {
            camera.stopRecordingVideo();
            capturePhoto.setColorFilter(ActivityCompat.getColor(MainActivity.this, android.R.color.white));
            capturePhoto.setEnabled(true);
            toggleCamera.setColorFilter(ActivityCompat.getColor(MainActivity.this, android.R.color.white));
            toggleCamera.setEnabled(true);
        }
    }

    @OnClick(R.id.toggleCamera)
    void toggleCamera() {
        switch (camera.toggleFacing()) {
            case CameraKit.Constants.FACING_BACK:
                Toast.makeText(this, "Switched to back camera!", Toast.LENGTH_SHORT).show();
                break;

            case CameraKit.Constants.FACING_FRONT:
                Toast.makeText(this, "Switched to front camera!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnClick(R.id.toggleFlash)
    void toggleFlash() {
//        camera.toggleFlashlight();
//        Log.d("whoa", camera.getCurrentRecordTime()+" ms. Currently recording? So "+camera.isRecording()+"!");
        switch (camera.toggleFlash()) {
            case CameraKit.Constants.FLASH_ON:
                Toast.makeText(this, "Flash on!", Toast.LENGTH_SHORT).show();
                break;

            case CameraKit.Constants.FLASH_OFF:
                Toast.makeText(this, "Flash off!", Toast.LENGTH_SHORT).show();
                break;

            case CameraKit.Constants.FLASH_AUTO:
                Toast.makeText(this, "Flash auto!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    RadioGroup.OnCheckedChangeListener captureModeChangedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            camera.setMethod(
                    checkedId == R.id.modeCaptureStandard ?
                            CameraKit.Constants.METHOD_STANDARD :
                            CameraKit.Constants.METHOD_STILL
            );

            Toast.makeText(MainActivity.this, "Picture capture set to" + (checkedId == R.id.modeCaptureStandard ? " quality!" : " speed!"), Toast.LENGTH_SHORT).show();
        }
    };

    RadioGroup.OnCheckedChangeListener zoomModeChangedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            zoom_seekBar.setVisibility(View.GONE);
            switch (checkedId) {
                case R.id.modeZoomOff:
                    camera.setZoom(CameraKit.Constants.ZOOM_OFF);
                    break;
                case R.id.modeZoomPinch:
                    camera.setZoom(CameraKit.Constants.ZOOM_PINCH);
                    break;
                case R.id.modeZoomSlider:
                    camera.setZoom(CameraKit.Constants.ZOOM_SLIDER);
                    camera.setSeekBar(zoom_seekBar);
                    zoom_seekBar.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    RadioGroup.OnCheckedChangeListener focusModeChangedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            focusMarker.setVisibility(View.GONE);
            switch (checkedId) {
                case R.id.modeFocusOff:
                    camera.setFocus(CameraKit.Constants.FOCUS_OFF);
                    break;
                case R.id.modeFocusContinuous:
                    camera.setFocus(CameraKit.Constants.FOCUS_CONTINUOUS);
                    break;
                case R.id.modeFocusTap:
                    camera.setFocus(CameraKit.Constants.FOCUS_TAP);
                    focusMarker.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    RadioGroup.OnCheckedChangeListener cropModeChangedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            camera.setCropOutput(
                    checkedId == R.id.modeCropVisible
            );

            Toast.makeText(MainActivity.this, "Picture cropping is" + (checkedId == R.id.modeCropVisible ? " on!" : " off!"), Toast.LENGTH_SHORT).show();
        }
    };

    RadioGroup.OnCheckedChangeListener qualityModeChangedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.modeQualityLow:
                    camera.setVideoProfile(CamcorderProfile.QUALITY_LOW);
                    break;
                case R.id.modeQuality480:
                    camera.setVideoProfile(CamcorderProfile.QUALITY_480P);
                    break;
                case R.id.modeQualityHigh:
                    camera.setVideoProfile(CamcorderProfile.QUALITY_HIGH);
                    break;
            }
        }
    };

    @OnTouch(R.id.focusMarker)
    boolean onTouchCamera(View view, MotionEvent motionEvent) {
        focusMarker.focus(motionEvent.getX(), motionEvent.getY());
        return false;
    }

    @OnClick(R.id.set_zoom_button)
    void setZoom() {
        try {
            camera.setZoomLevel(Integer.parseInt(edit_zoom.getText().toString()));
        } catch (Exception e) {

        }
        hideKeyboard();
    }

    @OnClick(R.id.button_flashlight)
    void toggleFlashLight(View v) {
        camera.toggleFlashlight();
        if (camera.isFlashlightOn()) {
            ((ImageView) v).setImageDrawable(ActivityCompat.getDrawable(this, R.drawable.ic_flash_off_black_24dp));
        } else {
            ((ImageView) v).setImageDrawable(ActivityCompat.getDrawable(this, R.drawable.ic_flash));
        }
    }

    @OnClick(R.id.button_info)
    void getInfo() {
        Toast.makeText(this, "Recording is " + (camera.isRecording() ? "on\nCurrent recording time: " : "off\nLast recording time: ") +
                camera.getCurrentRecordTime(), Toast.LENGTH_LONG).show();
    }

    void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
